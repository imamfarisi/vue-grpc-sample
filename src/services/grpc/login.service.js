import transport from '../../api/grpc'
import { LoginServiceClient } from '../../grpc/proto/login/login.client'

export const login = async (data) => {
    try {
        const client = new LoginServiceClient(transport)
        const { response } = await client.login(data)
        return response
    } catch (error) {
        return Promise.reject(error)
    }
}