import { GrpcWebFetchTransport } from "@protobuf-ts/grpcweb-transport";
import { BASE_URL } from "../constants/api.constant";

const transport = new GrpcWebFetchTransport({
    baseUrl: BASE_URL
})

export default transport
